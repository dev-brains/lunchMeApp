import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { AuthService } from "../../shared/services/api/auth.service";
import { SnackBar } from "nativescript-snackbar";
import { Store } from '../../utils';


@Component({
    selector: "login-app",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
    input: any;

    constructor(
        private router: RouterExtensions, 
        page: Page,
        private authService: AuthService
    ) {
        page.actionBarHidden = true;
        this.input = {
            email: "",
            password: ""
        };
    }

    ngOnInit(): void {
        if (this.authService.isAuthenticated) {
            this.router.navigate(["/home"], { clearHistory: true });
        }
    }

    login() {
        if (this.input.email && this.input.password) {
            const loginObject: ILogin = {
                email: this.input.email,
                password: this.input.password,
                returnSecureToken: true,
            }
            this.authService.login(loginObject)
                .subscribe( (data: any) => {
                    this.authService.isAuthenticated = true;
                    Store.setUser({
                        name: data.displayName,
                        email: data.email,
                        token: data.idToken,
                        expirationDateToken: data.expiresIn
                    })
                    this.router.navigate(['/home'], {
                        transition: {
                            name: 'slideLeft',
                            duration: 300,
                            curve: 'linear'
                        },
                        clearHistory: true
                    })

                },
                (dataError) => {
                    if (dataError && dataError.error && dataError.error.error.message === 'INVALID_PASSWORD') {
                        (new SnackBar()).simple("Wrong username of password. Please try again!");
                    }
                    else if (dataError && dataError.error && dataError.error.error.message === 'EMAIL_NOT_FOUND') {
                        (new SnackBar()).simple("your account doesn't exists, try to create a new one first!");
                    }
                })
        } else {
            (new SnackBar()).simple("All Fields Required!");
        }
    }
}
