import { Location } from "@angular/common";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";
import { Component, OnInit } from "@angular/core";
import { SnackBar } from "nativescript-snackbar";
import { Page } from "ui/page";
import { AuthService } from "../../shared/services/api/auth.service";
import { Store } from '../../utils';

@Component({
	selector: "register-app",
	moduleId: module.id,
	templateUrl: "./register.component.html",
	styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
	input: any;
	loading = false;
	
	constructor(
		private location: Location,
		private page: Page,
		private router: RouterExtensions,
		private authService: AuthService
	) {
		this.page.actionBarHidden = true;
		this.input = {
			firstname: "",
			lastname: "",
			email: "",
			password: ""
		};
		// Use the component constructor to inject providers.
	}

	register() {
		if (this.input.firstname && this.input.lastname && this.input.email && this.input.password) {
			this.loading = true;
			const registerObject: ILogin = {
				name: this.input.firstname + ' ' + this.input.lastname,
				email: this.input.email,
				password: this.input.password,
				returnSecureToken: true,
				displayName: this.input.firstname + ' ' + this.input.lastname
			}
			this.authService.register(registerObject)
				.subscribe((data: any) => {
					this.loading = false;
					if (!data.status) { // No status mean we have all the data from the currently logged in user (aka Success Call)
						this.authService.isAuthenticated = true;
						Store.setUser({
							name: registerObject.name,
							email: registerObject.email,
							token: data.idToken,
							expirationDateToken: data.expiresIn
						})
						this.router.navigate(['/home'], {
							transition: {
								name: 'slideLeft',
								duration: 300,
								curve: 'linear'
							},
							clearHistory: true
						})
					}
				})
		} else {
			this.loading = false;
			(new SnackBar()).simple("All Fields Required!");
		}
	}

	ngOnInit(): void {
		// Init your component properties here.
	}

	goBack() {
		this.location.back();
	}
}
