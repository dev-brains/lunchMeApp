import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";
import { PlacesService } from "../shared/services/api/places.service";
import { PullToRefresh } from "nativescript-pulltorefresh";

@Component({
	selector: "Home",
	moduleId: module.id,
	templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

	places: IPlace[];
	searchItems: any[];

	constructor(
		private router: RouterExtensions,
		private placesService: PlacesService,
	) { }

	ngOnInit(): void {
		// Init your component properties here.
		this.getAllPlaces()
	}

	onDrawerButtonTap(): void {
		const sideDrawer = <RadSideDrawer>app.getRootView();
		sideDrawer.showDrawer();
	}

	getAllPlaces(args?) {
		this.placesService.getAll()
			.subscribe( (res: any) => {
				this.places = res;
				if (args) {
					(<PullToRefresh>args.object).refreshing = false;
				}
			})
	}

	onItemTap(args): void {
		this.router.navigate(['/places', args.index], {
			transition: {
				name: 'slideLeft',
				duration: 300,
				curve: 'linear'
			},
			queryParams: {
				"place": JSON.stringify(this.places[args.index])
			}
		});
	}
}
