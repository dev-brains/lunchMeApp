import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export abstract class BaseApiService {

  constructor(protected http: HttpClient) {}
  
  abstract getBaseUrl(): string;

  getAll() {
    return this.http.get(this.getBaseUrl() + '.json')
  }

  getById(id) {
    return this.http.get(`${this.getBaseUrl()}/${id}.json`)
  }
}