import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '../../../utils';
import { RouterExtensions } from 'nativescript-angular/router';

@Injectable()
export class AuthService {

  private _isAuthenticated: boolean;

  constructor(
    protected http: HttpClient
  ) {}

  login(credentials: ILogin) {
    const loginUrl = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAw_-Vbw4aDkOxqKosXa7V-DyhBVtNf6jA'; // TODO: Replace this
    return this.http.post(loginUrl, credentials)
  }

  register(credentials: ILogin) {
    const registerUrl = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAw_-Vbw4aDkOxqKosXa7V-DyhBVtNf6jA'; // TODO: Replace this
    return this.http.post(registerUrl, credentials)
  }

  logout() {
    Store.clear();
    this.isAuthenticated = false;
  }

  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  public set isAuthenticated(isAuth: boolean) {
    this._isAuthenticated = isAuth;
  }
}