import { Injectable } from '@angular/core';
import { BaseApiService } from './base.api.service'
import { HttpClient } from '@angular/common/http';


@Injectable()
export class PlacesService extends BaseApiService {

	
	constructor(protected http: HttpClient) { 
		super(http);
	}
	
	getBaseUrl() {
		return 'https://lucnhmeapp.firebaseio.com/places';
	}
}