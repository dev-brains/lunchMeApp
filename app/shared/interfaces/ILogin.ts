interface ILogin {
  name?: string;
  email: string;
  password: string;
  returnSecureToken: boolean;
  displayName?: string;
}