interface IMenuItem {
  id?: string;
  name?: string;
  description?: string;
  photo?: string;
  rating?: string;
  price?: string;
}