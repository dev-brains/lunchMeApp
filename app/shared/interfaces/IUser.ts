interface IUser {
  name: string;
  email: string,
  token: string;
  expirationDateToken: string;
}