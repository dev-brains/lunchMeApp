interface IPlace {
  id?: number;
  name?: string;
  description?: string;
  rating?: number;
  location?: any;
  menu?: any;
  carousel?: any;
  mainImage?: string;
}