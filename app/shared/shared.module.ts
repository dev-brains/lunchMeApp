import { NgModule } from '@angular/core';

import { ActionBarComponent } from './action-bar/action-bar.component';
import { PlacesService } from './services/api/places.service';

@NgModule({
  imports: [],
  exports: [],
  declarations: [ActionBarComponent],
  providers: [],
})
export class SharedModule { }
