import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PlaceComponent } from './place.component';
import { PlaceRoutingModule } from './place.routing.module';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  imports: [
    NativeScriptCommonModule,
    PlaceRoutingModule
  ],
  declarations: [
    PlaceComponent,
    MenuComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class PlaceModule { }
