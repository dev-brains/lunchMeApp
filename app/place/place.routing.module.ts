import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { PlaceComponent } from './place.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  { path: ':id', component: PlaceComponent },
  { path: ':id/menu', component: MenuComponent }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule],
})

export class PlaceRoutingModule { }
