import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlacesService } from '../../shared/services/api/places.service';

@Component({
  selector: 'menu-item',
  moduleId: module.id,
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  id: number;
  menuItems: IMenuItem[];

  constructor(
    private route: ActivatedRoute,
    private placesService: PlacesService
  ) {
    // this.route.params
    //   .forEach((params) => { this.id = +params["id"]; });
    this.route.queryParams.subscribe(params => {
        console.log(params["menuItems"])
    });
  }

  ngOnInit() { 
    this.menuItems = [
      {
        name: 'Tacos',
        description: 'Tacolgando',
        photo: 'http://via.placeholder.com/10x10',
        rating: '5',
        price: '$55'
      },
      {
        name: 'Enchiladas',
        description: 'De mole poblano del monet',
        photo: 'http://via.placeholder.com/10x10',
        rating: '5',
        price: '$55'
      },
      {
        name: 'Pozole',
        description: 'Mamalon',
        photo: 'http://via.placeholder.com/10x10',
        rating: '5',
        price: '$55'
      },
      {
        name: 'Aguachile',
        description: 'Bien picoso alv',
        photo: 'http://via.placeholder.com/10x10',
        rating: '5',
        price: '$55'
      },
      {
        name: 'Tortas',
        description: 'Torton',
        photo: 'http://via.placeholder.com/10x10',
        rating: '5',
        price: '$55'
      }
    ]
  }
}