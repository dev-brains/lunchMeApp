import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlacesService } from '../shared/services/api/places.service';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'place-list',
  moduleId: module.id,
  templateUrl: './place.component.html'
})
export class PlaceComponent implements OnInit {

  id: number;
  place: IPlace;
	loading = false;

  constructor(
    private route: ActivatedRoute,
    private router: RouterExtensions,
    private placesService: PlacesService
  ) {
    this.route.queryParams
      .forEach((params) => { 
        this.place = JSON.parse(params["place"]);
      });
  }

  ngOnInit() {}

  onCheckMenu() {
    this.router.navigate(['menu'], {
			transition: {
				name: 'slideLeft',
				duration: 300,
				curve: 'linear'
      },
      relativeTo: this.route
		});
  }
}