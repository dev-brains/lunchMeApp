const appSettings = require("application-settings");

export class Store {

  constructor() {}

  static setUser(user: IUser) {
    appSettings.setString('user', JSON.stringify(user));
  }

  static getUser(): IUser {
    return appSettings.get('user') ? JSON.parse(appSettings.get('user')) : {};
  }

  static clear() {
    appSettings.clear();
  }
}