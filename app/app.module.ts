import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { HttpClientModule } from '@angular/common/http'; 
import { registerElement} from "nativescript-angular/element-registry";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlacesService } from "./shared/services/api/places.service";
import { AuthService } from "./shared/services/api/auth.service";

registerElement("PullToRefresh",() => require("nativescript-pulltorefresh").PullToRefresh);

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        HttpClientModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        PlacesService,
        AuthService
    ]
})
export class AppModule { }
